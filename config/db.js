const mongoose = require("mongoose");
const colors = require("colors");

const connectToDb = async () => {
  try {
    await mongoose.connect(`${process.env.MONGO_URI}`, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });

    console.log("MongoDB connected!".inverse.cyan);
  } catch (err) {
    console.log("Failed to connect to MongoDB".red, err);
  }
};

module.exports = connectToDb;
